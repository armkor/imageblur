#include <QCoreApplication>
#include <QImage>
#include <QFileInfo>
#include <iostream>
#include <QtTest/QtTest>
#include <QThread>
#include <stdlib.h>
#include "functions.h"
#include "inputOutput.h"

using namespace std;

int main(int argc, char *argv[])
{
	InputOutput inpout;

	bool status = true;
	status &= inpout.checkFillInput(argc, argv);

	imageToGray(inpout.getImage(), inpout.getThreadCount());
	imageGaussian(inpout.getImage(), inpout.getThreadCount());
	status &= inpout.saveImage();
	return status;
}
