#include <QImage>

struct workingData
{
	QImage* img;
	int x1;
	int x2;
	int y1;
	int y2;
};

struct gaussianData
{
	QImage* img;
	QImage* resImg;
	int x1;
	int x2;
	int y1;
	int y2;
	double** matrix;
	int matrixSize;
};


void imageToGray(QImage* img, int threadsCount);

void imageToGrayPartly(workingData pixelCoords);

void makeGaussian(double** matrix);

void imageGaussian(QImage* img, int threadsCount);

void doGaussian(gaussianData address);
