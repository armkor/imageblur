QT += gui

CONFIG += c++11 console
CONFIG -= app_bundle
CONFIG  += qtestlib
QT += concurrent

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        functions.cpp \
        inputOutput.cpp \
        main.cpp
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    functions.h \
    inputOutput.h

DISTFILES += \
    lenna.jpg
