#include "functions.h"
#include <QFuture>
#include <QVector>
#include <QImage>
#include <QtConcurrent>

static int matrixSize = 10;


using namespace std;

void imageToGray(QImage* img, int threadsCount)
{
	int dy = img->height() / threadsCount;
	int y = 0;
	QVector<workingData> tasks;

	while( y < img->height() - dy)
	{
		tasks << workingData{ img, 0, img->width(), y, y + dy };
		y += dy;
	}

	tasks << workingData{ img, 0, img->width(), y, img->height() };

	QFuture<void> future = QtConcurrent::map(tasks, imageToGrayPartly);
	future.waitForFinished();
}

void imageToGrayPartly(workingData pixelCoords)
{
	const int depth = 4;

	for (int y = pixelCoords.y1; y < pixelCoords.y2; y++) {
		uchar* scan = pixelCoords.img->scanLine(y);

		for (int x = pixelCoords.x1; x < pixelCoords.x2; x++) {

			QRgb* rgbpixel = reinterpret_cast<QRgb*>(scan + x * depth);
			int gray = qGray(*rgbpixel);
			*rgbpixel = QColor(gray, gray, gray).rgba();
		}
	}
}


void makeGaussian(double** matrix)
{
	const int rows = matrixSize;
	const int columns = matrixSize;
	const double sigma = 0.84089642;

	double sum = 0.0;


	for (int i = 0 ; i < rows; i++) {
		for (int j = 0 ; j < columns; j++) {
			matrix[i][j] = exp( - ( pow(i, 2) + pow(j, 2) ) / (2 * pow( sigma, 2) ) ) / ( 2 * M_PI * pow(sigma, 2) );
			sum += matrix[i][j];
		}
	}

	for (int i = 0 ; i < rows ; i++) {
		for (int j = 0 ; j < columns ; j++) {
			matrix[i][j] /= sum;
		}
	}
}

void imageGaussian(QImage* img, int threadsCount)
{
	double** gaussian = new double* [matrixSize];
	for (int i = 0; i < matrixSize; i++)
		gaussian[i] = new double [matrixSize];
	makeGaussian(gaussian);


	QImage* resultImg = new QImage(img->width(), img->height(), img->format());

	int dy = img->height() / threadsCount;
	int y = 0;

	QVector<gaussianData> tasks;

	while( y < img->height() - dy)
	{
		tasks << gaussianData{ img,resultImg, 0, img->width(), y, y + dy, gaussian, matrixSize };
		y += dy;
	}

	tasks << gaussianData{ img, resultImg, 0, img->width(), y, img->height(), gaussian, matrixSize };

	QFuture<void> future = QtConcurrent::map(tasks, doGaussian);
	future.waitForFinished();

	*img = resultImg->copy();
	delete resultImg;

	for (int i = 0; i < matrixSize; i++)
		delete[] gaussian[i];
	delete[] gaussian;
}




void doGaussian(gaussianData address)
{
	for (int y = address.y1; y < address.y2; y++) {
		for (int x = address.x1; x < address.x2; x++) {

			QRgb pixel = address.img->pixel(x, y);

			double red = 0;
			double blue = 0;
			double green = 0;


			for(int i = 0; i < address.matrixSize; i++)
				for(int j = 0; j < address.matrixSize; j++)
				{
					red += qRed(pixel) * address.matrix[i][j];
					blue += qBlue(pixel) * address.matrix[i][j];
					green += qGreen(pixel) * address.matrix[i][j];

				}

			address.resImg->setPixel(x, y, qRgb(red, green, blue));
		}
	}
}
