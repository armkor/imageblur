#include <QString>
#include <QFileInfo>
#include <QImage>

class InputOutput
{
public:
	bool checkFillInput(int argc, char *argv[]);

	QImage* getImage();
	bool saveImage();

	int getThreadCount();

private:

	bool readImage();
	bool fillThreadCount();

	int m_argc;
	char** m_argv;

	std::unique_ptr<QImage> m_image;
	QFileInfo m_imageFilePath;
	int m_threadCount;
};






