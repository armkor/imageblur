#include "inputOutput.h"
#include <iostream>
#include <memory>
#include <QThread>

bool InputOutput::checkFillInput(int argc, char *argv[])
{
	if (!( 2 == argc || 3 == argc))
	{
		std::cout << "Usage: <path to file> [OPTIONAL number of cores]" << std::endl;
		return false;
	}

	m_argc = argc;
	m_argv = argv;

	bool status = true;
	status &= fillThreadCount();
	status &= readImage();
	return status;
}

bool InputOutput::readImage()
{
	m_imageFilePath = QFileInfo(m_argv[1]);

	if (!m_imageFilePath.exists())
	{
		std::cout << "Can't find file: " << m_imageFilePath.canonicalFilePath().toStdString() << std::endl;
		return false;
	}

	m_image = std::make_unique<QImage>(m_imageFilePath.canonicalFilePath());

	if (m_image.get()->isNull())
	{
		std::cout << "Error while loading image. Image is empty." << std::endl;
		return false;
	}

	return true;
}

bool InputOutput::fillThreadCount()
{
	m_threadCount =  QThread::idealThreadCount();

	if (3 == m_argc)
	{
		const int inThreadCount = atoi(m_argv[2]);
		if (inThreadCount <= 0)
		{
			std::cout << "number of cores > 1 is needed" << std::endl;
			return false;
		}
		else
		{
			m_threadCount = inThreadCount;
		}
	}
	return true;
}

QImage* InputOutput::getImage()
{
	return m_image.get();
}

bool InputOutput::saveImage()
{
	const QString newPath = QString("%1/%2_result.%3")
			.arg(m_imageFilePath.path())
			.arg(m_imageFilePath.baseName())
			.arg(m_imageFilePath.completeSuffix());

	std::cout << "Saving image.." << std::endl;

	const bool st = m_image.get()->save(newPath);
	(st)? std::cout << "Image saved" << std::endl : std::cout << "Error while saving image" << std::endl;
	return st;
}

int InputOutput::getThreadCount()
{
	return m_threadCount;
}

